import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment.prod";
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { IArticulos } from '../interfaces/interfaces';
declare function menu();

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {
  // carritoLocal : Array<IArticulos> = [];
  index: number;
  carro: IArticulos[] = [];
  carroHeader: EventEmitter<IArticulos[]> = new EventEmitter();
  mostrarMenuEvent: EventEmitter<boolean> = new EventEmitter();
  mostrarMenu: boolean = false;
  constructor(private http: HttpClient) { }

  getAllArticles(): Observable<IArticulos[]> {
    return this.http.get<IArticulos[]>(`${environment.SERVER}/articulo/productos/nuevos`);
  }
  addCarEvent(articulo : IArticulos){
    let tf = this.carro.map((art, i) => {
      if(art.nombre == articulo.nombre){
        this.index = i;
        return true;
      }
    });
    // let x = tf.some(valor => valor);
    
    this.carro.push(articulo);
    this.agregarCarrito();
    this.carroHeader.emit(this.carro);
  }
  agregarCarrito(carro: IArticulos[] = this.carro) {
    localStorage.setItem('carro', JSON.stringify(carro));
    this.carroHeader.emit();
  }

  verifiarcarrito(){
    const carro = JSON.parse( localStorage.getItem('carro'));
    !carro ? console.log('no hay nada') : this.carro = carro;
  }

  getLocal(): IArticulos[] {
    const CARRO = JSON.parse( localStorage.getItem('carro') );
    return CARRO;
  }

  getArticle(id: string): Observable<IArticulos> {
    return this.http.get<IArticulos>(`${environment.SERVER}/articulo/${id}`);
  }

  uploadImage(imagenes: FileList, id: string): Observable<any> {
    const formData = new FormData();
    for (let i = 0; i < imagenes.length; i++) {
      formData.append(`imagen`, imagenes.item(i));
    }
    
    return this.http.post<any>(`${environment.SERVER}/articulo/imagen/${id}`, formData);
  }
  
  //esto es lo mismo que tenemos en precioentre
  articulosRelacionados(termino: string): Observable<IArticulos>  { 
    return this.http.get<IArticulos>(`${environment.SERVER}/articulo/similar/x/${termino}/prueba`).pipe( tap(data => console.log(data)));
  }

  menux(){
    if( this.mostrarMenu ){
      console.log('debe de servir');
    } else {
      menu();
    }
  }

  imagenes( articulos: IArticulos[] ): IArticulos[] {
    let articulosCorrecto = articulos.map((articulo) => {
      if(articulo.img.length > 0){
        
        articulo.img = articulo.img[0];
      }
      return articulo
    });
    return articulosCorrecto;
  }

  // se utiliza 
  popularidad(tipo: string):Observable<any[]>  {
    return this.http.get<any[]>(`${ environment.SERVER }/articulo/popularidad/${tipo}`);
  }

  ordenarPor(tipo: string, orden: number): Observable<any[]>{
    return this.http.post<any[]>(` ${ environment.SERVER }/articulo/ordenarpor/${tipo}`, {orden});
  }

  getArticlesStartt(tipo:string): Observable<any[]>{
    return this.http.get<any[]>(` ${ environment.SERVER }/articulo/todos/${tipo} `)
  }

  getArticlesFisrt(): Observable<any[]>{
    return this.http.get<any[]>(` ${ environment.SERVER }/articulo/articulosTodos/inicio`);
  }

  articuloEntre(tipo: string, minimo: number, maximo: number):Observable<any[]> {
    return this.http.post<any[]>(` ${ environment.SERVER }/articulo/articulo/entre/${tipo}`, {minimo, maximo});
  }

  comprarArticulo(articulos: IArticulos[]): Observable<any>{
    return this.http.put(` ${environment.SERVER}/articulo/vendidos/articulos`, {articulos}).pipe( tap(data => console.log(data)) );
  }

  eliminarArticulo(id: string):Observable<any> {
    return this.http.delete<any>( ` ${ environment.SERVER }/articulo/${id} ` );
  }
  getOnlyOneArticulo(id: string): Observable<IArticulos>{
    return this.http.get<IArticulos>(` ${ environment.SERVER }/articulo/${id}`);
  }

  actualizarArticle( id: string, articulo: IArticulos): Observable<any> {
    return this.http.put<any>( ` ${ environment.SERVER }/articulo/${id}`, {articulo});
  }

}

