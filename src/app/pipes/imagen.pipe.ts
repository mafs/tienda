import { Pipe, PipeTransform } from '@angular/core';
import { environment } from "../../environments/environment.prod";

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(value: string): string {
    // console.log(value);
    // return 'http://localhost:3000/img/item-02.jpg'
    return `${environment.SERVER}/articulo/imagen/${value}`;
  }

}
