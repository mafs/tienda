import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./inicio/login/login.component";
import { RegisterComponent } from "./inicio/register/register.component";


const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '', redirectTo: '/inicio/dashboard', pathMatch: 'full' }
];

export const APP_ROUTES_ROOT = RouterModule.forRoot(APP_ROUTES);
