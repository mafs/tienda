import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

//rutas
import { APP_ROUTES_ROOT } from './app.routes';

//esta es la pagina de inicio
import { PagesModule } from './pages/pages.module';
import { LoginComponent } from './inicio/login/login.component';
import { RegisterComponent } from './inicio/register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    // CommonModule,
    BrowserModule,
    PagesModule,
    APP_ROUTES_ROOT
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
