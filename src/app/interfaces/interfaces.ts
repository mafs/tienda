// control /name Microsoft.CredentialManager
// cambiar password de git
export interface IArticulos {
    nombre: string;
    tipo: string;
    color: string;
    talla: string;
    cantidad: number;
    cantidadTotal: number;
    precio: number;
    img?: string[] | string;
    genero: string;
    fecha: Date;
    descuento?: number;
    _id: string;
};
