import { Component, OnInit } from '@angular/core';
declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function todoslick();

@Component({
  selector: 'app-ver-carro',
  templateUrl: './ver-carro.component.html',
  styles: []
})
export class VerCarroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    todo();
    iniciarPagina();
    productoDetalles();
    todoslick();
  }

}
