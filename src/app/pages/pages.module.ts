import { NgModule } from '@angular/core';

//para el ngFor
import { CommonModule } from '@angular/common';

//sirve para los ngmodel
import { FormsModule }   from '@angular/forms';

import { HttpClientModule } from "@angular/common/http";

import { InicioComponent } from './inicio/inicio.component';
import { PAGES_ROUTES_MODULE } from './pages.routes';
import { HeaderComponent } from '../shared/header/header.component';
import { CaruseComponent } from './caruse/caruse.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { NuevoArticulosComponent } from './nuevo-articulos/nuevo-articulos.component';
import { FooterComponent } from './footer/footer.component';

import { NguCarouselModule } from '@ngu/carousel';
import { CarroComponent } from './carro/carro.component';

//pipe
import { ImagenPipe } from '../pipes/imagen.pipe';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductosComponent } from './productos/productos.component';

//slider
import { Ng5SliderModule } from 'ng5-slider';

//pagination
import {NgxPaginationModule} from 'ngx-pagination';

//reactiveForms
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdministrarComponent } from './administrar/administrar.component';
import { EditarArticuloComponent } from './editar-articulo/editar-articulo.component';


@NgModule({
  imports: [
    CommonModule,
    PAGES_ROUTES_MODULE,
    FormsModule,
    NguCarouselModule,
    HttpClientModule,
    Ng5SliderModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    InicioComponent,
    HeaderComponent,
    CaruseComponent,
    DashboardComponent,
    ArticulosComponent,
    NuevoArticulosComponent,
    FooterComponent,
    CarroComponent,
    ImagenPipe,
    ProductDetailComponent,
    ProductosComponent,
    AdministrarComponent,
    EditarArticuloComponent,
  ],
  exports: [
    InicioComponent,
    HeaderComponent,
    CaruseComponent,
    ImagenPipe
  ]
})
export class PagesModule { }
