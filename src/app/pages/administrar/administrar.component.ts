import { Component, OnInit, ElementRef } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';
import { IArticulos } from 'src/app/interfaces/interfaces';
import { Router } from '@angular/router';
declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function todoslick();

@Component({
  selector: 'app-administrar',
  templateUrl: './administrar.component.html',
  styles: []
})
export class AdministrarComponent implements OnInit {

  total: number;
  articulos: IArticulos[] = [];
  modal: string = 'swal-overlay';
  nombre: string = '';
  id: string = '';
  tituloModal: string = '';
  cuerpoModal: string = '';
  botonModal: string = 'cancelar';
  mostrarBotones: boolean = true;
  p: number = 1;
  constructor(
    private _articuloServices: ArticulosService,
    private router: Router
    ) { }

  ngOnInit() {
    productoDetalles();
    todo();
    iniciarPagina();
    todoslick();
    this.getAllArticles();
  }
  getAllArticles(){
    this._articuloServices.getAllArticles().subscribe((data: any) => {
      this.total = data.total;
      this.articulos = this._articuloServices.imagenes(data.resp);
    });
  }

  abrirModal(art: IArticulos){
    this.modal = 'swal-overlay swal-overlay--show-modal';
    this.nombre = art.nombre;
    this.tituloModal = 'eliminar';
    this.cuerpoModal = 'se va a eliminar'
    this.botonModal = 'eliminar';
    this.id = art._id;
  }

  cerrarmodal(eliminar: boolean){
    if(eliminar){
      this._articuloServices.eliminarArticulo( this.id ).subscribe(data => {
        this.mostrarBotones = false;
        this.botonModal = 'ok';
        this.modal = 'swal-overlay swal-overlay--show-modal';
        this.cuerpoModal = 'se elimino correctamenta';
        // console.log(data);
      }, err => console.log(err));
    }
    this.modal = 'swal-overlay';
  }

  //paginacion
  pageChanged( event: ElementRef ) {
    this.p = Number(event);
    console.log(event);
  }
  editar(id: string) {
    this.router.navigate(['/inicio/editar/', id]);
  }

}


