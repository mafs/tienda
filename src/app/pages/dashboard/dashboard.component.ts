import { Component, OnInit } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';
declare function todo();
declare function iniciarPagina();
declare function todoslick();
// declare function menu();
// declare function remover();

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  constructor( private _articuloServices: ArticulosService ) { }

  ngOnInit() {
    iniciarPagina();
    todo();
    todoslick();
    // menu();
  }

  mostraMenu(){
    this._articuloServices.menux();
    this._articuloServices.mostrarMenu = true;
  }
  
  
  

}
