import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';
import { IArticulos } from 'src/app/interfaces/interfaces';


@Component({
  selector: 'app-nuevo-articulos',
  templateUrl: './nuevo-articulos.component.html',
  styles: []
})
export class NuevoArticulosComponent implements OnInit {
  modal: string = 'swal-overlay';
  articulos: IArticulos[] = [];
  nombre: string;
  id: string = '';
  @Input() buscar: string = '';
  constructor(private _articulosServices: ArticulosService) { this.getAllArticles(); }

  ngOnInit() {
    this.getAllArticles();
    //verifica el localstoraage para asignarlo a la variable
    this._articulosServices.verifiarcarrito();
  }

  getAllArticles() {
    if(this.buscar.length == 0){
      this._articulosServices.getAllArticles().subscribe((data: any) => {
        this.articulos = this._articulosServices.imagenes(data.resp);
      });
    } else {
      this._articulosServices.articulosRelacionados(this.buscar).subscribe((resp:any) => this.articulos = resp.articulo);
    }
  }

  agregarCarrito(articulo: IArticulos){
    this.nombre = articulo.nombre;
    this._articulosServices.addCarEvent(articulo);
    this.modal = 'swal-overlay swal-overlay--show-modal';
  }
  cerrarmodal(){
    this.modal = 'swal-overlay';
  }
}
