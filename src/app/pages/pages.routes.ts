import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./inicio/inicio.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CaruseComponent } from "./caruse/caruse.component";
import { CarroComponent } from "./carro/carro.component";
import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { ProductosComponent } from "./productos/productos.component";
import { AdministrarComponent } from "./administrar/administrar.component";
import { EditarArticuloComponent } from "./editar-articulo/editar-articulo.component";

const PAGES_ROUTES : Routes = [
    {   
        path: 'inicio',
        component: InicioComponent,
        children : [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'carro', component: CarroComponent },
            { path: 'producto-detail/:id', component: ProductDetailComponent },
            { path: 'productos/:tipo', component: ProductosComponent },
            { path: 'administrar', component: AdministrarComponent },
            { path: 'editar/:id', component: EditarArticuloComponent }
        ]
    }
];

export const PAGES_ROUTES_MODULE = RouterModule.forChild(PAGES_ROUTES);