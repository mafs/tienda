import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig  } from '@ngu/carousel';

@Component({
  selector: 'app-caruse',
  templateUrl: './caruse.component.html',
  styles: [`.otro {
    width: 100%;
    height: 400px;
    overflow: hidden;
  }
  .middle {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }`]
})
export class CaruseComponent implements OnInit {
  img: string = 'src/assets/images/2.png';
  imgags = [
    'src/assets/images/2.png',
    'src/assets/images/3.png',
    'src/assets/images/5.png'
  ];
  public carouselTileItems: Array<any> = [0];
  public carouselTiles = {
    0: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
    slide: 1,
    speed: 250,
    point: {
      visible: true
    },
    load: 2,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)',
  };

  constructor() { }

  ngOnInit() {
    this.carouselTileItems.forEach(el => {
      // regresa solo un numero 0
      console.log(el, 'el');
      
      this.carouselTileLoad(el);
    });
  }

  carouselTileLoad(j) {
    const len = this.carouselTiles[j].length;    
    // 0
    if (len <= 5) {
      for (let i = len; i < len + 5; i++) {
        console.log(i);
        this.carouselTiles[j].push(this.imgags[i]);
      }
    }
  }
  panter() {
    console.log('hhhhhhhhhhhh');
  }
}



