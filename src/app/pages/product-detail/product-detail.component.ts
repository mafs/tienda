import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ArticulosService } from 'src/app/services/articulos.service';
import { IArticulos } from 'src/app/interfaces/interfaces';
declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function menu();
declare function todoslick();

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styles: []
})
export class ProductDetailComponent implements OnInit {

  id: string;
  articulo: IArticulos;
  imagenes: string[] = [];
  x: any;
  constructor(
    private route: ActivatedRoute,
    private _articuloServices: ArticulosService
    ) { }

  ngOnInit() {
    iniciarPagina();
    this.obtenerId();
    // this.obtenerArticulo();
    productoDetalles();
    // todo();
    todoslick();
  }

  obtenerId(){
    this.route.params.subscribe(data => {
      this.id = data.id;
      this.obtenerArticulo();
    });
  }

  obtenerArticulo(){
    this._articuloServices.getArticle(this.id).subscribe((data: any) => {
      this.articulo = data.articulo;
      console.log(this.articulo, 'esto son los articulos');
      this.imagenes = data.articulo.img;
      this.obtenerArticulosRelacionados(data.articulo.tipo);
    });
  }

  archivos(files: FileList) {
    this._articuloServices.uploadImage(files, this.id).subscribe(resp => console.log(resp));
  }

  agregarCarro(articulo: IArticulos) {
    
    this.x = articulo;
    this.x.img = this.x.img[0];
    if(this.x.img.includes('.')){
      console.log('entro');
      
    }
    this._articuloServices.addCarEvent(this.x);
  }

  obtenerArticulosRelacionados(termino: string) {
    this._articuloServices.articulosRelacionados(termino).subscribe(resp => console.log(resp));
  }

  mostrarMenu(){
    this._articuloServices.menux();
  }

}
