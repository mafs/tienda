import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { IArticulos } from 'src/app/interfaces/interfaces';
import { ArticulosService } from 'src/app/services/articulos.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function todoslick();

@Component({
  selector: 'app-editar-articulo',
  templateUrl: './editar-articulo.component.html',
  styles: [`.text-danger{
    color: red;
  }`]
})
export class EditarArticuloComponent implements OnInit {

  id:string = '';
  articulo: IArticulos;
  articuloForm: FormGroup
  constructor(
    private route: ActivatedRoute,
    private _articuloServices: ArticulosService,
    private fb: FormBuilder,
    private router: Router
    ) { }

  ngOnInit() {
    this.getId();
    this.getArticle();
    productoDetalles();
    todo();
    iniciarPagina();
    todoslick();
    this.formGroup();
    this.articuloForm.get('descuento').valueChanges.pipe(debounceTime(1000)).subscribe( data => {
      if(!data){
        this.articuloForm.get('descuento').patchValue(0);
      }
    });
    this.articuloForm.get('precio').valueChanges.pipe(debounceTime(1000)).subscribe( data => {
      if(data == 0){
        this.articuloForm.get('precio').patchValue(1);
      }
    });
  }

  getId(){
    this.route.paramMap.subscribe((data: any) => this.id = data.params.id);
  }

  getArticle() {
    this._articuloServices.getOnlyOneArticulo(this.id).subscribe(
      (data: any) => {
        if( data.articulo.img.length > 0 ){
          data.articulo.img = data.articulo.img[0]
        }
        this.articulo = data.articulo;
        console.log( this.articulo );
      }, err => console.error(err),
      () => this.agregarDatosForm());
  }

  formGroup() {
    this.articuloForm = this.fb.group({
      nombre: ['', Validators.required],
      tipo: ['', Validators.required],
      cantidadTotal: ['', Validators.required],
      precio: ['', [Validators.required, Validators.min(100)]],
      descuento: ['', [Validators.required, Validators.max(100)]],
      genero: ['', [Validators.required]],
      color: ['', Validators.required],
      talla: ['', Validators.required]
    });
  }
  agregarDatosForm(){
    this.articuloForm.setValue ({
      nombre: this.articulo.nombre,
      tipo: this.articulo.tipo,
      cantidadTotal: this.articulo.cantidadTotal,
      precio: 500,
      // precio: this.articulo.precio,
      descuento: this.articulo.descuento,
      genero: 6,
      color: 'fff',
      talla: 'm'
    });
  }
  enviar(){
    this._articuloServices.actualizarArticle(this.id, this.articuloForm.value).subscribe(
      data => console.log(data),
      err => console.log(err),
      () =>  console.log('se ejecuta'));
    // console.log( this.articuloForm.value );
  }
  regresar(){
    this.router.navigate(['/inicio/administrar']);
  }

  archivos( imagenes: FileList){
    this._articuloServices.uploadImage( imagenes, this.id ).subscribe(data => console.log(data));
  }
}


// (ngSubmit)="onSubmit()"

// onSubmit() {
//   // TODO: Use EventEmitter with form value
//   console.warn(this.profileForm.value);
// }


    // color: string;
    // talla: string;
    // cantidad: number;
    // cantidadTotal: number;
    // precio: number;
    // img?: string[] | string;
    // genero: string;
    // descuento?: number;
    // _id: string;

    // nombre: { type: String, required: [true, 'el campo nombre es requerido'] },
    // tipo: { type: String, required: [true, 'el campo tipo es requerido'] },
    // color: { type: String, required: [true, 'el campo color es requerido'] },
    // talla: { type: String, required: [true, 'el campo talla es requerido'], enum: tallasValidas },
    // cantidad: { type: Number, default: 1 ,required: [true, 'el campo cantidad es requerido'] },
    // cantidadTotal: { type: Number, required: [true, 'el campo cantidad TOTAL es requerido'] },
    // precio: { type: Number, required: [true, 'el campo precio es requerido'] },
    // genero: { type: String, required: [true, 'el campo genero es requerido'] },