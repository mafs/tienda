import { Component, OnInit } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';
import { IArticulos } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styles: []
})
export class ArticulosComponent implements OnInit {
  
  articulos: IArticulos[] = [];
  constructor(private _articuloServices: ArticulosService) { }

  ngOnInit() {
    this.getAllArticlesFirst();
  }
  
  getAllArticlesFirst() {
    this._articuloServices.getArticlesFisrt().subscribe(data => this.articulos = data);
  }

}
