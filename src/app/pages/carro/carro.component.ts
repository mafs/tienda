import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { IArticulos } from 'src/app/interfaces/interfaces';
import { ArticulosService } from 'src/app/services/articulos.service';
import { TimeInterval } from 'rxjs/internal/operators/timeInterval';

declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function todoslick();

@Component({
  selector: 'app-carro',
  templateUrl: './carro.component.html',
  styles: []
})
export class CarroComponent implements OnInit {

  articulos: IArticulos[] = [];
  precio: number = 0;
  precioTotal: number[] = [];
  modal: string = 'swal-overlay';  
  tituloModal = '';
  bodyModal = '';
  fuera: IArticulos[] = [];
  constructor(private _articulosServices: ArticulosService) { }

  ngOnInit() {
    this.getArticlesFromLocal();
    productoDetalles();
    todo();
    iniciarPagina();
    todoslick();
  }
  uno(i: number, n: number) {
    const input: any = document.getElementsByClassName('c')[i];
    if(n > 0){
      this.articulos[i].cantidad += 1;
      if(this.articulos[i].cantidad >= 10){
        this.articulos[i].cantidad = 10
      }
    } else {
      this.articulos[i].cantidad -= 1;
      if(this.articulos[i].cantidad < 1){
        this.articulos[i].cantidad = 1;
        input.value = this.articulos[i].cantidad;
      }
    }
    this.sumarTotal();
  }

  dos(evento: any, i: number){
    if(evento >= 10){
      this.articulos[i].cantidad = 10
    } else if(evento == ''){
      this.articulos[i].cantidad = 1;
    }
    this.sumarTotal();
  }
  getArticlesFromLocal() {
    this.articulos = this._articulosServices.getLocal();
    this.sumarTotal();
  }


  sumarTotal() {
    this.articulos.map(articulo => {      
      // console.log(articulo.precio, articulo.cantidad);
       let total = articulo.precio * articulo.cantidad;
       this.precioTotal.push(total);
       total = 0;
    });
    this.precio = 0;
    this.precioTotal.map(numero => {
      this.precio += numero;
    });
    this.precioTotal = [];
  }
  eliminar(articulo: IArticulos){
    // console.log(articulo);
    
    const index = this.articulos.indexOf(articulo);
    // console.log(index);
    
    let articuloFuera = this.articulos.splice(index, 1)[0];
    // console.log(articuloFuera);
    this._articulosServices.agregarCarrito(this.articulos);
    this.sumarTotal();
  }

  comprar() {
    this.articulos.map((art:any) => console.log(art._id));
    this.tituloModal = 'Compras';
    this.bodyModal = 'la compra se realizp con exito';
    this._articulosServices.comprarArticulo( this.articulos ).subscribe(data => this.modal = 'swal-overlay swal-overlay--show-modal', err => console.log(err));
  }

  cupon() {
    this.tituloModal = 'Cupon';

    this.bodyModal = 'Cupon Valido';
    this.modal = 'swal-overlay swal-overlay--show-modal'
  }

  cerrarmodal(){
    this.modal = 'swal-overlay';
  }
}