import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IArticulos } from 'src/app/interfaces/interfaces';
import { ArticulosService } from 'src/app/services/articulos.service';
import { Options } from 'ng5-slider';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

declare function productoDetalles();
declare function todo();
declare function iniciarPagina();
declare function menu();
declare function todoslick();

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: [`productos.component.scss`]
})
export class ProductosComponent implements OnInit {

  tipo: string;
  total: number;
  inicio: number = 1;
  articulos: IArticulos[] = [];
  fechaHoy: string = new Date().toISOString();

  p: number = 1;
  value: number = 0;
  highValue: number = 3000;
  options: Options = {
    floor: 0,
    ceil: 3000,
    step: 50,
    translate: (value: number): string => {
      return '$' + value;
    }
  };
  constructor( private route: ActivatedRoute,
    private _articuloServices: ArticulosService,
    private fb: FormBuilder) { }

    buscarForm: FormGroup = this.fb.group({
      buscar: ['']
    });

  ngOnInit() {
    todo();
    iniciarPagina();
    productoDetalles();
    this.getTipo();
    todoslick();
    this.updateNameForm();
    this.getArticles();
  }

  getTipo() {
    this.route.params.subscribe(data => {
      this.tipo = data.tipo;
    });
  }

  getArticles(tipo:string = this.tipo){
    this._articuloServices.getArticlesStartt( tipo ).subscribe((data: any) => {
      this.total = data.total;
      this.articulos = this._articuloServices.imagenes(data.resp);
    });
  }

  //aqui comparo fcha
  compararNew( fecha: any ): boolean {
    const mes = new Date(fecha).getMonth();
    const year = new Date(fecha).getFullYear();
    const mesActual =  new Date().getMonth();
    const yaerActual =  new Date().getFullYear();
    if(mes == mesActual && year == yaerActual){
      return true;
    }
    return false;
  }

  // aqui comparo el total
  math():number  {
    return Math.ceil(this.total / 12)
  }

  //paginacion
  pageChanged( event: ElementRef ) {
    this.p = Number(event);
    console.log(event);
  }

  //busca atravez del form
  updateNameForm(): void {
    this.buscarForm.get('buscar').valueChanges.pipe(debounceTime(1000)).subscribe( termino => {      
      if(termino.length > 0){
        this._articuloServices.articulosRelacionados(termino).subscribe((data: any) => {
          this.articulos = this._articuloServices.imagenes(data.resp);
          this.total = data.total;
        });
      }else {
        this._articuloServices.articulosRelacionados('nada').subscribe((data: any) => {
          this.articulos = this._articuloServices.imagenes(data.resp);
          this.total = data.total;
        });
      }
    });    
  }

  buscar( data: string ) {
    
    switch(data){
      case "1":
      this._articuloServices.popularidad(this.tipo).subscribe((data: any) => {
        this.total = data.total;
        this.articulos = this._articuloServices.imagenes(data.resp);
      });
        break;
      case "2":
        this._articuloServices.ordenarPor( this.tipo, 1 ).subscribe((data: any) => {
          this.total = data.total;
          this.articulos = this._articuloServices.imagenes(data.resp);
        });
        break;
      case "3":
        this._articuloServices.ordenarPor( this.tipo, -1 ).subscribe((data: any) => {
          this.total = data.total;
          this.articulos = this._articuloServices.imagenes(data.resp);
        });
        break;
      default:
        console.log('default');
        
    }
  }
  valores() {
    this._articuloServices.articuloEntre( this.tipo, this.value, this.highValue).subscribe((data: any) => {
      this.total = data.total;
      this.articulos = this._articuloServices.imagenes(data.resp);
    });
  }
  agregarCarro(articulo: IArticulos){
    // console.log(articulo);
    this._articuloServices.addCarEvent(articulo);
  }
}

