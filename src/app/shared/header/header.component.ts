import { Component, OnInit } from '@angular/core';
import { ArticulosService } from 'src/app/services/articulos.service';
import { IArticulos } from 'src/app/interfaces/interfaces';
declare function menu();

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
  .active {
    color: #e65540;
  }
  `]
})
export class HeaderComponent implements OnInit {

  articulos: IArticulos[] = [];
  precioTotal: number = 0;
  constructor(private _articuloService: ArticulosService) { }

  ngOnInit() {
    this.getArticles();
    this._articuloService.carroHeader.subscribe(() => {
      console.log('dddddddddddddd');
      this.articulos = this._articuloService.getLocal();
    });
  }
  getArticles(){
    setTimeout(() => {
      this.articulos = this._articuloService.carro.reverse();
      console.log(this.articulos);
      if(this.articulos.length == 0){
        this.articulos = this._articuloService.getLocal();
        console.log( this.articulos );
      }
      this.sumarTotal();
    }, 550);
  }
  sumarTotal() {
    this.precioTotal = 0;
    this.articulos.map(articulo => {
      articulo.precio = articulo.precio * articulo.cantidad;
      this.precioTotal += articulo.precio;
    });
  }
  eliminar(articulo: IArticulos){
    const index = this.articulos.indexOf(articulo);
    this.articulos.splice(index, 1);
    this._articuloService.agregarCarrito(this.articulos);
    this.sumarTotal();
  }
  mostrarMenu() {
    if(!this._articuloService.mostrarMenu){
      menu()
    }
    this._articuloService.mostrarMenu = true;
  }
}
